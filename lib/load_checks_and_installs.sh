#!/usr/bin/env bash
# vim :ft = bash:

set -eu

load_library_functions () {
  local script_dir
  script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd -P)"

  # shellcheck disable=SC1090
  {
    . "${script_dir}/std/messages.sh"
    . "${script_dir}/std/utils.sh"
  }
}


# Provide default default, always true
check_ () {
  return 0
}


pkg_install () {
  local pkgs=$*

  case "$(uname)" in
    Darwin*)
      if command -v brew>/dev/null; then
        # shellcheck disable=SC2086
        brew install $pkgs
      else
        fatal "No brew installation utility available!"
      fi
      ;;
    *)
      # shellcheck disable=SC2086
      sudo DEBIAN_INSTALLER=noninteractive apt-get -y install $pkgs
      ;;
  esac
}


load_scripts () {
  local st=$1
  local file

  case $st in
    check|install)
      for file in "${DATA_DIR}/scripts/${st}"/* ; do
        # shellcheck disable=SC1090
        . "$file"
      done
      ;;
    *)
      error "Unrecognised script type ${st}; no action taken"
      ;;
  esac
}


load_library_functions

load_scripts check
load_scripts install

#!/bin/bash
#
# This is a library to simplify output of messages

load_library_functions () {
  local script_path
  local script_dir
  if command -v realpath >>/dev/null; then
    script_path=$(realpath "${BASH_SOURCE[0]}")
  else
    script_path="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd -P)"
  fi
  script_dir=$(dirname "${script_path}")

  # shellcheck disable=SC1090
  {
    . "${script_dir}/colors.sh"
  }
}

load_library_functions

MSG_COLOR_DEBUG=$COLOR_WHITE
MSG_COLOR_EMPH=$COLOR_WHITE
MSG_COLOR_INFO=$COLOR_DEFAULT
MSG_COLOR_WARN=$COLOR_YELLOW
MSG_COLOR_ERROR=$COLOR_RED
MSG_COLOR_FATAL="${COLOR_BLACK}${BGCOLOR_RED}"

debug () {
  [[ ${DEBUG:-0} -eq 0 ]] && return 0
  printf "${MSG_COLOR_EMPH}%s${COLOR_NC}\n" "$1"
}

emph () {
  [[ ${QUIET:-0} -gt 0 ]] && return 0
  printf "${MSG_COLOR_EMPH}%s${COLOR_NC}\n" "$1"
}

info () {
  [[ ${QUIET:-0} -gt 0 ]] && return 0
  printf "${MSG_COLOR_INFO}%s${COLOR_NC}\n" "$1"
}

warn () {
  [[ ${QUIET:-0} -gt 0 ]] && return 0
  printf "${MSG_COLOR_WARN}%s${COLOR_NC}\n" "$1"
}

error () {
  printf "${MSG_COLOR_ERROR}%s${COLOR_NC}\n" "$1"
}

fatal () {
  printf "${MSG_COLOR_FATAL}%s${COLOR_NC}\n" "$1"
}


set_emph_color () {
  MSG_COLOR_EMPH=$1
}
set_info_color () {
  MSG_COLOR_INFO=$1
}
set_warn_color () {
  MSG_COLOR_WARN=$1
}
set_error_color () {
  MSG_COLOR_ERROR=$1
}
set_fatal_color () {
  MSG_COLOR_FATAL=$1
}

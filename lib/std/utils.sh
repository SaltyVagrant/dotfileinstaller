#!/usr/bin/env bash
# vi :set ft=bash:

faketty () {
  script -qefc "$(printf "%q " "$@")" /dev/null
}
